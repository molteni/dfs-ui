import Vue from 'vue'
import Router from 'vue-router'
import Main from './components/Main.vue'
import ProductDetail from './components/product/ProductDetail.vue'
import CompanyMain from './components/company/CompanyMain.vue'
import CompanyEdit from './components/company/CompanyEdit.vue'
import ProductEdit from './components/product/ProductEdit.vue'
import Login from "./components/security/Login";
import store from "./store"


Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/detail',
      name: 'detail',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: ProductDetail
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: Login
    },
    {
      path: '/company',
      name: 'companyMain',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: CompanyMain,
      children: [
        {
          // UserProfile will be rendered inside User's <router-view>
          // when /user/:id/profile is matched
          path: 'edit',
          component: CompanyEdit
        }]
    },
    {
      path: '/product/edit',
      name: 'predictEdit',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: ProductEdit
    }
  ]});

router.beforeEach((to, from, next) => {
  let currentUser = store.state.user;

  let requiresAuth = !["main", "login"].includes(to.name);
  if (requiresAuth && !currentUser) next('login');
  else if (!requiresAuth && currentUser) next();
  else next()
});

export default router