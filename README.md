# dsf-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve 
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### build Docker
```
docker build -t javaee/dfs-app .
```

### run Docker
```
docker run -it -p 8080:8080 --rm --name dfs-app-1 javaee/dfs-app
```

### Deploy to GitLabs
```
docker build -t registry.gitlab.com/molteni/dfs-ui .
```

```
docker push registry.gitlab.com/molteni/dfs-ui
```